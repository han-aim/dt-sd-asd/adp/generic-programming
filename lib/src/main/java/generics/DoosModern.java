package generics;

/** https://docs.oracle.com/en/java/javase/17/language/records.html */
record DoosModern<Item extends Comparable<? super Item>>(Item waarde)
    implements Comparable<DoosModern<Item>> {
  @Override
  public int compareTo(DoosModern<Item> andere) {
    return waarde.compareTo(andere.waarde());
  }
}
