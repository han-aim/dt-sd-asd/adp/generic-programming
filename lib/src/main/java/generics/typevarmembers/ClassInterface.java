// Source: https://docs.oracle.com/javase/specs/jls/se17/html/jls-4.html#jls-4.4
package generics.typevarmembers;

class ClassInterface extends Class implements Interface {
  public void methodInterface() {}
}
