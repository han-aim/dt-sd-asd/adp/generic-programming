package generics;

import java.util.ArrayList;

class Anders {
  /**
   * Vind laagste waarde in een rij.
   *
   * @param rij een rij
   * @return laagste waarde
   */
  public static <Item extends Comparable<? super Item>> Item laagste(Item[] rij) {
    Item itemLaagste = rij[0];
    for (Item item : rij) {
      if (item.compareTo(itemLaagste) < 0) {
        itemLaagste = item;
      }
    }

    return itemLaagste;
  }

  /**
   * Vind laagste waarde in een dynamische rij.
   *
   * @param rij een dynamische rij
   * @return laagste waarde
   */
  public static <Item extends Comparable<? super Item>> Item laagste(ArrayList<Item> rij) {
    Item itemLaagste = rij.get(0);
    for (Item item : rij) {
      if (item.compareTo(itemLaagste) < 0) {
        itemLaagste = item;
      }
    }

    return itemLaagste;
  }
}
