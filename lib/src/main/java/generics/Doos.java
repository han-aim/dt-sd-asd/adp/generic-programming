package generics;

class Doos<Item extends Comparable<? super Item>> implements Comparable<Doos<Item>> {
  private Item waarde;

  public Item lees() {
    return waarde;
  }

  public void schrijf(Item waarde) {
    this.waarde = waarde;
  }

  @Override
  public int compareTo(Doos<Item> andere) {
    return waarde.compareTo(andere.lees());
  }

  @Override
  public String toString() {
    return waarde.toString();
  }
}
