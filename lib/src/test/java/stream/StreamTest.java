package stream;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.stream.IntStream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class StreamTest {
  @ParameterizedTest
  @CsvSource({"212,32432,334,343,67"})
  void test1(final int a, final int b, final int c, final int d, final int e) {
    final long[] gemiddeldeCollect =
        IntStream.of(a, b, c, d, e)
            .collect(
                () -> new long[3],
                (gemiddeldeStaat, term) -> {
                  // teller
                  gemiddeldeStaat[0]++;
                  // som
                  gemiddeldeStaat[1] += term;
                  // gemiddelde
                  gemiddeldeStaat[2] =
                      (term + (gemiddeldeStaat[0] - 1) * gemiddeldeStaat[2]) / gemiddeldeStaat[0];
                },
                // TODO: weghalen
                (gemiddeldeStaat, gemiddeldeStaat2) -> {});
    final double gemiddeldeAverage = IntStream.of(a, b, c, d, e).average().getAsDouble();
    System.err.println(
        "gemiddeldeCollect: teller: "
            + gemiddeldeCollect[0]
            + ", som: "
            + gemiddeldeCollect[1]
            + ", gemiddelde: "
            + gemiddeldeCollect[2]);
    System.err.println("gemiddeldeAverage: " + gemiddeldeAverage);
    assertEquals(gemiddeldeCollect[2], gemiddeldeAverage);
  }
}
