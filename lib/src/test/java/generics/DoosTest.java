package generics;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvSource;

class DoosTest {

  @ParameterizedTest
  @CsvSource({"a,b,c"})
  void test1(final String a, final String b, final String c) {
    final Doos<String> doosA, doosB, doosC;
    doosA = new Doos<>();
    doosB = new Doos<>();
    doosC = new Doos<>();
    doosA.schrijf(a);
    doosB.schrijf(b);
    doosC.schrijf(c);
    // Rijvariant. Een primitieve rij kan items van het type Item maar ook subklasses van Item
    // bevatten (primitieve rijen zijn covariant). Java moet at runtime nog precies weten welk type
    // item de rij heeft, om voldoende geheugen te reserveren. Echter, deze informatie is voor
    // generics alleen at compile time beschikbaar.
    // Zie: https://dzone.com/articles/covariance-and-contravariance
    // Daarom moeten we een raw type gebruiken en de compilerwaarschuwingen aanvaarden:
    Doos[] input = {doosC, doosB, doosA};
    Doos<String> laagste = Anders.laagste(input);
    assertEquals(laagste, doosA);
  }

  @ParameterizedTest
  @CsvSource({"a,b,c"})
  void test2(final String a, final String b, final String c) {
    final Doos<String> doosA, doosB, doosC;
    doosA = new Doos<>();
    doosB = new Doos<>();
    doosC = new Doos<>();
    doosA.schrijf(a);
    doosB.schrijf(b);
    doosC.schrijf(c);
    // ArrayList-variant. Geeft geen waarschuwingen:
    ArrayList<Doos> lijst = new ArrayList<Doos>();
    lijst.add(doosA);
    lijst.add(doosB);
    lijst.add(doosC);
    Doos<String> laagste = Anders.laagste(lijst);
    assertEquals(laagste, doosA);
  }
}
