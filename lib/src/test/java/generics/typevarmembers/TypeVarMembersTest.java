package generics.typevarmembers;

import org.junit.jupiter.api.Test;

class TypeVarMembersTest {

  private <Type extends Class & Interface> void test(Type value) {
    value.methodInterface();
    value.methodClassPublic();
    value.methodClassProtected();
    value.methodClassPackage();
    // Compile-time error
    // t.methodClassPrivate();
  }

  @Test
  void testGenericprogramming() {
    final ClassInterface classInterface = new ClassInterface();
    test(classInterface);
  }
}
